# C39
=======

![C39](https://i.imgur.com/KuWIIuW.png)

A compact 39 key keyboard.

Keyboard Maintainer: [Maple Computing]()  
Hardware Supported: C39 PCB  
Hardware Availability: [SpaceCat.design](https:\\spacecat.design)

Make example for this keyboard (after setting up your build environment):

    make c39:default

See [build environment setup](https://docs.qmk.fm/build_environment_setup.html) then the [make instructions](https://docs.qmk.fm/make_instructions.html) for more information.
